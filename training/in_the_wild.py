def configure_in_the_wild_datasets():
    phase_cfg = {
        TRAIN: {'dsnames': args.dataset_train,
                'count': args.train_count},
        VAL: {'dsnames': args.dataset_val,
              'count': args.val_count}
    }
    datasets = {}
    for phase in args.phases:
        dsnames = phase_cfg[phase]['dsnames']
        if dsnames is None:
            continue
        num_samples = phase_cfg[phase]['count']
        is_single_dataset = isinstance(dsnames, str) or len(dsnames) == 1
        train = phase == TRAIN
        datasets_for_phase = []
        for name in dsnames:
            root, cache_root = cfg.get_dataset_paths(name)
            transform = ds_utils.build_transform(deterministic=not train, daug=args.daug)
            if args.edge_target:
                target_transform = CannyFilter(args.canny_min_val)
            elif args.output_channels == 1:
                target_transform = ToGrayscale()
            else:
                target_transform = None

            dataset_cls = cfg.get_dataset_class(name)
            ds = dataset_cls(root=root,
                             cache_root=cache_root,
                             train=train,
                             max_samples=num_samples,
                             use_cache=args.use_cache,
                             start=args.st if train else None,
                             test_split=args.test_split,
                             align_face_orientation=args.align,
                             crop_source=args.crop_source,
                             transform=transform,
                             target_transform=target_transform,
                             output_size=args.output_size,
                             image_size=args.input_size,
                             with_occlusion=args.occ and train)
            datasets_for_phase.append(ds)
        if is_single_dataset:
            datasets[phase] = datasets_for_phase[0]
        else:
            datasets[phase] = multi.ConcatFaceDataset(datasets_for_phase, max_samples=args.train_count_multi)