import json
import os

import numpy as np
import torch


def count_parameters(m):
    return sum(p.numel() for p in m.parameters() if p.requires_grad)


def to_numpy(ft):
    if isinstance(ft, np.ndarray):
        return ft
    try:
        return ft.detach().cpu().numpy()
    except AttributeError:
        return None


def to_image(m):
    img = to_numpy(m)
    if img.shape[0] == 3:
        img = img.transpose((1, 2, 0)).copy()
    return img


def unsqueeze(x):
    if isinstance(x, np.ndarray):
        return x[np.newaxis, ...]
    else:
        return x.unsqueeze(dim=0)


def atleast4d(x):
    if len(x.shape) == 3:
        return unsqueeze(x)
    return x


def atleast3d(x):
    if len(x.shape) == 2:
        return unsqueeze(x)
    return x


class Batch:

    def __init__(self, data, n=None, gpu=True, eval=False):
        self.images = atleast4d(data['image'])

        self.eval = eval

        try:
            self.ids = data['id']
            try:
                if self.ids.min() < 0 or self.ids.max() == 0:
                    self.ids = None
            except AttributeError:
                self.ids = np.array(self.ids)
        except KeyError:
            self.ids = None

        try:
            # self.target_images = data['target']
            self.target_images = atleast4d(data['target_image'])
        except KeyError:
            self.target_images = None

        try:
            self.target_landmarks = atleast3d(data['target_landmarks'])
        except KeyError:
            self.target_landmarks = None

        try:
            self.face_heights = data['face_heights']
        except KeyError:
            self.face_heights = None

        try:
            self.poses = data['pose']
        except KeyError:
            self.poses = None

        try:
            self.landmarks = atleast3d(data['landmarks'])
        except KeyError:
            self.landmarks = None

        try:
            self.clips = np.array(data['vid'])
        except KeyError:
            self.clips = None

        try:
            self.fnames = data['fnames']
        except:
            self.fnames = None

        try:
            self.bumps = data['bumps']
        except:
            self.bumps = None

        try:
            self.affine = data['affine']
        except:
            self.affine = None

        try:
            self.face_masks = data['face_mask']
        except:
            self.face_masks = None

        if self.face_masks is not None:
            self.face_weights = self.face_masks.float()
            if not self.eval:
                self.face_weights += 1.0
            self.face_weights /= self.face_weights.max()
            # plt.imshow(self.face_weights[0,0])
            # plt.show()

            # if cfg.WITH_FACE_MASK:
            #     mask = self.face_masks.unsqueeze(1).expand_as(self.images).float()
            #     mask /= mask.max()
            #     self.images *= mask

        self.lm_heatmaps = None

        try:
            # self.face_weights = data['face_weights']
            self.lm_heatmaps = data['lm_heatmaps']
            if len(self.lm_heatmaps.shape) == 3:
                self.lm_heatmaps = self.lm_heatmaps.unsqueeze(1)
        except KeyError:
            self.face_weights = 1.0

        try:
            self.occ_boxes = data["occ_boxes"]
        except KeyError:
            self.occ_boxes = None

        try:
            self.target_occ_boxes = data["target_occ_boxes"]
        except KeyError:
            self.target_occ_boxes = None

        for k, v in self.__dict__.items():
            if v is not None:
                try:
                    self.__dict__[k] = v[:n]
                except TypeError:
                    pass

        if gpu:
            for k, v in self.__dict__.items():
                if v is not None:
                    try:
                        self.__dict__[k] = v.cuda()
                    except AttributeError:
                        pass

    def __len__(self):
        return len(self.images)


class VideoBatch:
    def __init__(self, data, gpu):

        self.frames = data["input_frames"]

        try:
            self.target_frames = data["target_frames"]
        except KeyError:
            self.target_frames = None

        try:
            self.video_masks = data["mask"]
        except KeyError:
            self.video_masks = None

        if gpu:
            for k, v in self.__dict__.items():
                if v is not None:
                    try:
                        self.__dict__[k] = v.cuda()
                    except AttributeError:
                        pass

    def __len__(self):
        return len(self.frames)


class MixedBatch:
    def __init__(self, im_data, video_data, n=None, gpu=True, eval=False):
        self.eval = eval

        if im_data is not None:
            self.images = atleast4d(im_data['image'])

            try:
                self.ids = im_data['id']
                try:
                    if self.ids.min() < 0 or self.ids.max() == 0:
                        self.ids = None
                except AttributeError:
                    self.ids = np.array(self.ids)
            except KeyError:
                self.ids = None

            try:
                # self.target_images = data['target']
                self.target_images = atleast4d(im_data['target_image'])
            except KeyError:
                self.target_images = None

            try:
                self.target_landmarks = atleast3d(im_data['target_landmarks'])
            except KeyError:
                self.target_landmarks = None

            try:
                self.face_heights = im_data['face_heights']
            except KeyError:
                self.face_heights = None

            try:
                self.poses = im_data['pose']
            except KeyError:
                self.poses = None

            try:
                self.landmarks = atleast3d(im_data['landmarks'])
            except KeyError:
                self.landmarks = None

            try:
                self.clips = np.array(im_data['vid'])
            except KeyError:
                self.clips = None

            try:
                self.fnames = im_data['fnames']
            except:
                self.fnames = None

            try:
                self.bumps = im_data['bumps']
            except:
                self.bumps = None

            try:
                self.affine = im_data['affine']
            except:
                self.affine = None

            try:
                self.face_masks = im_data['face_mask']
            except:
                self.face_masks = None

            if self.face_masks is not None:
                self.face_weights = self.face_masks.float()
                if not self.eval:
                    self.face_weights += 1.0
                self.face_weights /= self.face_weights.max()
                # plt.imshow(self.face_weights[0,0])
                # plt.show()

                # if cfg.WITH_FACE_MASK:
                #     mask = self.face_masks.unsqueeze(1).expand_as(self.images).float()
                #     mask /= mask.max()
                #     self.images *= mask

            self.lm_heatmaps = None

            try:
                # self.face_weights = data['face_weights']
                self.lm_heatmaps = im_data['lm_heatmaps']
                if len(self.lm_heatmaps.shape) == 3:
                    self.lm_heatmaps = self.lm_heatmaps.unsqueeze(1)
            except KeyError:
                self.face_weights = 1.0

            try:
                self.occ_boxes = im_data["occ_boxes"]
            except KeyError:
                self.occ_boxes = None

            try:
                self.target_occ_boxes = im_data["target_occ_boxes"]
            except KeyError:
                self.target_occ_boxes = None

        if video_data is not None:
            self.frames = video_data["input_frames"]

            try:
                self.target_frames = video_data["target_frames"]
            except KeyError:
                self.target_frames = None

            try:
                self.video_masks = video_data["mask"]
            except KeyError:
                self.video_masks = None

        for k, v in self.__dict__.items():
            if v is not None:
                try:
                    self.__dict__[k] = v[:n]
                except TypeError:
                    pass

        if gpu:
            for k, v in self.__dict__.items():
                if v is not None:
                    try:
                        self.__dict__[k] = v.cuda()
                    except AttributeError:
                        pass

    def __len__(self):
        if hasattr(self, 'images') and self.images is not None:
            return len(self.images)
        else:
            return len(self.frames)



def set_requires_grad(model, requires_grad):
    for param in model.parameters():
        param.requires_grad = requires_grad


def read_model(in_dir, model_name):
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

    filepath_mdl = os.path.join(in_dir, model_name+'.mdl')
    snapshot = torch.load(filepath_mdl,  map_location=device)
    return snapshot
    # try:
    #     model.load_state_dict(snapshot['state_dict'], strict=False)
    # except RuntimeError as e:
    #     print(e)


def read_meta(in_dir):
    with open(os.path.join(in_dir, 'meta.json'), 'r') as outfile:
        data = json.load(outfile)
    return data


def denormalized_video(tensor):
    t = tensor.clone()
    t[:, :, 0] += 0.518
    t[:, :, 1] += 0.418
    t[:, :, 2] += 0.361
    return t


def denormalize(tensor):
    # assert(len(tensor.shape[1] == 3)
    if tensor.shape[1] == 3:
        tensor[:, 0] += 0.518
        tensor[:, 1] += 0.418
        tensor[:, 2] += 0.361
    elif tensor.shape[-1] == 3:
        tensor[..., 0] += 0.518
        tensor[..., 1] += 0.418
        tensor[..., 2] += 0.361


def denormalized(tensor):
    # assert(len(tensor.shape[1] == 3)
    if isinstance(tensor, np.ndarray):
        t = tensor.copy()
    else:
        t = tensor.clone()
    denormalize(t)
    return t