import sys
from pathlib import Path
import numpy as np
import glob
import os
import pandas as pd
from scipy.io import loadmat

par_dir = Path(__file__).resolve().parents[1]
sys.path.append(os.path.join(par_dir))

from csl_common.utils import geometry
from datasets.additional_datasets import facedataset
# import config as cfg


class CASIA(facedataset.FaceDataset):

    def __init__(self, root, cache_root=None, crop_source='bb_ground_truth',
                 return_modified_images=False, min_face_height=80, **kwargs):

        assert (crop_source in ['bb_ground_truth', 'lm_ground_truth', 'lm_openface'])

        fullsize_img_dir = os.path.join(root, "images")

        super().__init__(root=root,
                         cache_root=cache_root,
                         fullsize_img_dir=fullsize_img_dir,
                         crop_source=crop_source,
                         crop_dir=cache_root,
                         return_landmark_heatmaps=False,
                         return_modified_images=return_modified_images,
                         **kwargs)

        self.min_face_height = min_face_height

        # shuffle images since dataset is sorted by identities
        import sklearn.utils
        self.annotations = sklearn.utils.shuffle(self.annotations)

        print("Number of images: {}".format(len(self)))
        print("Number of identities: {}".format(self.annotations.ID.nunique()))

    def _load_annotations(self, split):
        ann_path = os.path.join(self.root, "annotations.pkl")
        if os.path.isfile(ann_path):
            annotations = pd.read_pickle(ann_path)
        else:
            ann = []
            id_dirs = glob.glob(os.path.join(self.fullsize_img_dir,"*"))

            for dir_path in id_dirs:
                celeb_id = os.path.basename(dir_path)
                for filename in os.listdir(dir_path):
                    fname = celeb_id + "/" + filename
                    ann.append({"fname": fname, "ID": celeb_id})
            annotations = pd.DataFrame(ann)
            annotations.to_pickle(ann_path)

        return annotations

    @property
    def labels(self):
        return self.annotations.ID.values

    @staticmethod
    def _get_identity(sample):
        return sample.ID

    def __len__(self):
        return len(self.annotations)

    def __getitem__(self, idx):
        sample = self.annotations.iloc[idx]
        bb = [0, 0, 249, 249]
        # bb = self.get_adjusted_bounding_box(sample.X, sample.Y, sample.W, sample.H)
        # bb = [sample.X, sample.Y, sample.X + sample.W, sample.Y + sample.H]
        bb = geometry.extend_bbox(bb, dt=-0.05, db=-0.10, dl=-0.05, dr=-0.05)

        return self.get_sample(sample.fname, bb)


# cfg.register_dataset(CASIA)